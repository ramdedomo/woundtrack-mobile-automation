@delete
Feature: Assessment

  Scenario Outline: Delete assessment

    Given I am on the login page

        When I login with 'ramdedomo' and 'Tester2023!'
        Then i should be redirected to Patient list

    Given I am on the Patient List

        When i search for 'William'
        And i delete the assessment '06/01/2023'
        Then it should be deleted