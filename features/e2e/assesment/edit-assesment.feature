@edit
Feature: Assessment

  Scenario Outline: Editing a new assessment

    Given I am on the login page

        When I login with 'ramdedomo' and 'Tester2023!'
        Then i should be redirected to Patient list

    Given I am on the Patient List

        When i search for 'William'
        # must change the date greater than the previous date
        And i edit the '07/08/2023' assessment date and time to '07/09/2023' and '11:30'
        Then it should be updated
