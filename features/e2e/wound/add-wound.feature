@wound-add
Feature: Assessment

  Scenario Outline: Beginning a new assessment

    Given I am on the login page

        When I login with 'ramdedomo' and 'Tester2023!'
        Then i should be redirected to Patient list

    Given I am on the Patient List

        # Right Head/Left Head => Face, Skull, Eye, Forehead, Ear, Chin, Nose, Neck, Mouth, Back of Head
        # Right Upper Limp/Left Upper Limp => Arm, Wrist, Forearm, Hand, Elbow, Palm, Antecubital, Finger, Axilla, Other
        # Right Trunk/Left Trunk => Shoulder, Chest, Breast, Abdomen, Pubis, Genitalia, Hip, Buttock, Back, Groin, Lumbar, Sacrum
        # Right Lower Limb/Left Lower Limb => Thigh, Leg, Knee, Popliteal, Calf, Ankle, Foot, Heel, Sole, Toes

        When i search for 'William'
        And I open '06/31/2023' assessment
        # must greater than previous assessment date
        Then a wound location page should show

        When i add a new wound
        When i tap the area 'Right Head' specifically 'Face'
        Then a add new wound page should show

        When i fill up the fields for adding a new wound
        Then it should be filled up

    


