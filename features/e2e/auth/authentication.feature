@auth
Feature: Login

  Scenario Outline: Login using given credentials

    Given I am on the login page

    When I login with 'ramdedomo' and 'Tester2023!'
    Then i should be redirected to Patient list

    When i click logout
    Then i should be redirected to Login page
    
    
