@settings
Feature: Settings

  Scenario Outline: Navigate Settings

    Given I am on the login page

    When I login with 'ramdedomo' and 'Tester2023!'
    Then i should be redirected to Patient list

    Given Settings Tab

    #open instruction
    When I click instruction
    Then a instruction page should show

    #open user agreement
    When I click user agreement
    Then a user agreement page should show

    #open link to demo
    When I click link to demo
    Then a link to demo video should play