class Menu{
    async clickMenuItems(menu){ //menu selection
        await driver.pause(2000)
        switch (menu) {
            case 1:
                await $('~Patients\nTab 1 of 3').click()
                await $('~Patients\nTab 1 of 3').isEnabled()
                break; 
            case 2:
                await $('~Settings\nTab 2 of 3').click()
                await $('~Settings\nTab 2 of 3').isEnabled()
                break;
            case 3:
                await $('~Logout\nTab 3 of 3').click()
                break;
            default:
                break;
        }
    }
}

const menu = new Menu()
export default menu