import findSelector from "../functions/findSelector.js";

class Patients{
    async checkPatientList(){
        await $('//android.view.View[@content-desc="Patients"]').isExisting();
    }
    async findPatient(name){
        let find = await $('//android.widget.EditText')
        await find.click()
        await driver.pause(5000)
        await find.setValue(name)
    }
    async clickPatient(name){
        let user = await $(`//android.view.View[contains(@content-desc, "${name}")]`)
        await user.click()
    }
    async clickBeginAssessment(){
        let begin = await $('//android.widget.Button[@content-desc="Begin Assessment"]')
        await begin.waitForDisplayed({ timeout: 20000 });
        await begin.click()
    }
    async clickNewAssessment(){
        await $('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.Button').click()
    }
    async setDate(when){
        await $('//android.widget.Button[@content-desc="Switch to input"]').click()
        let date = await $('//android.widget.EditText')
        await date.click()
        await driver.pause(500)
        await date.setValue(when)
        await $('//android.widget.Button[@content-desc="SET"]').click()
        await driver.pause(3000)
    }
    async openAssessmentDetails(current){
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        function convertDate(date_str) {
            let temp_date = date_str.split("/");
            return months[Number(temp_date[0]) - 1] + " " + temp_date[1] + ", " + temp_date[2];
        }
        let assessment
        assessment = await $(`//android.view.View[contains(@content-desc, '${convertDate(current)}')]/android.widget.Button[2]`)
        await assessment.isExisting()

        if(assessment){
            await $(`//android.view.View[contains(@content-desc, '${convertDate(current)}')]/android.widget.Button[1]`).click()
        }else{
            await $(`//android.view.View[contains(@content-desc, '${convertDate(current)}')]/android.widget.Button[2]`).click()
        }

    }
    async editAssessment(current, date, time){
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        function convertDate(date_str) {
            let temp_date = date_str.split("/");
            return months[Number(temp_date[0]) - 1] + " " + temp_date[1] + ", " + temp_date[2];
        }

        let assessment = await $(`//android.view.View[contains(@content-desc, '${convertDate(current)}')]`)
        let assessmentExist = await assessment.isExisting()
        while (!assessmentExist) {
            await driver.touchAction([
                {action: 'press', x: 87, y: 1351},
                {action: 'moveTo', x: 87, y: 612},
                'release'
            ]);

            assessmentExist = await assessment.isExisting()
        }
        await assessment.click()

        await $(`//android.widget.Button[@content-desc="Set Date:"]`).click()
        await driver.pause(1000)
        this.setDate(date)

        await driver.pause(2000)

        let assessment2 = await $(`//android.view.View[contains(@content-desc, '${convertDate(date)}')]`)
        let assessment2Exist = await assessment2.isExisting()
        while (!assessment2Exist) {
            await driver.touchAction([
                {action: 'press', x: 87, y: 1351},
                {action: 'moveTo', x: 87, y: 612},
                'release'
            ]);

            assessment2Exist = await assessment2.isExisting()
        }
        await assessment2.click()


        await $(`//android.widget.Button[@content-desc="Set Time:"]`).click()   
        this.setTime(time)
        await $(`//android.widget.Button[@content-desc="OK"]`).click()   
    }

    async setTime(time){

        await driver.pause(2000)

        let h = time.split(':')[0]
        let m = time.split(':')[1]

        let hour = await $(`//android.widget.SeekBar[contains(@content-desc, "Select hours")]`)
        await hour.click()
        await driver.pause(1000)
        await hour.setValue(String(h))

        await driver.pause(1000)

        let minute = await $(`//android.widget.SeekBar[contains(@content-desc, "Select minutes")]`)
        await minute.click()
        await driver.pause(1000)
        await minute.setValue(String(m))

        await driver.pause(1000)
        await $(`//android.widget.RadioButton[@content-desc="AM"]`).click()   
    }
    async openAssessment(date){
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        function convertDate(date_str) {
            let temp_date = date_str.split("/");
            return months[Number(temp_date[0]) - 1] + " " + temp_date[1] + ", " + temp_date[2];
        }

        let assessment = await $(`//android.view.View[contains(@content-desc, '${convertDate(date)}')]/android.widget.Button`)
        await assessment.click()
    }
    async deleteAssessment(date){
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        function convertDate(date_str) {
            let temp_date = date_str.split("/");
            return months[Number(temp_date[0]) - 1] + " " + temp_date[1] + ", " + temp_date[2];
        }

        let assessment = await $(`//android.view.View[contains(@content-desc, '${convertDate(date)}')]/android.widget.Button[1]`)
        await assessment.click()
        await $(`//android.widget.Button[@content-desc="Yes"]`).click()
    }
    async newWoundYes(){
        let yes = await $(`//android.widget.Button[@content-desc="YES"]`)
        let yesExist = await yes.isExisting()
        
        while(!yesExist){
            await driver.touchAction([
                {action: 'press', x: 156, y: 1340},
                {action: 'moveTo', x: 156, y: 670},
                'release'
            ]);
    
            yesExist = await yes.isExisting()
        }

        await yes.click()
    }
    async newWoundNo(){
        await $(`//android.widget.Button[@content-desc="NO"]`).click()
    }
    async checkTapArea(){
        await $('//android.view.View[@content-desc="Tap the area where the wound is"]').isExisting();
    }
    async clickWoundArea(area){
        await driver.pause(100)
        switch (area) {
            case "Right Head":
                let rh = await $('//android.view.View[@content-desc="Right\nHead"]')
                await driver.pause(100)
                await rh.click()
                await driver.pause(100)
                await rh.click()
                break;
            case "Left Head":
                let lh = await $('//android.view.View[@content-desc="Left\nHead"]')
                await driver.pause(100)
                await lh.click()
                await driver.pause(100)
                await lh.click()
                break;
            case "Right Trunk":
                let rt = await $('//android.view.View[@content-desc="Right\nTrunk"]')
                await driver.pause(100)
                await rt.click()
                await driver.pause(100)
                await rt.click()
                break;
            case "Left Trunk":
                let lt = await $('//android.view.View[@content-desc="Left\nTrunk"]')
                await driver.pause(100)
                await lt.click()
                await driver.pause(100)
                await lt.click()
                break;
            case "Right Upper Limb":
                let rul = await $('//android.view.View[@content-desc="Right\nUpper Limb"]')
                await driver.pause(100)
                await rul.click()
                await driver.pause(100)
                await rul.click()
                break;
            case "Left Upper Limb":
                let lul = await $('//android.view.View[@content-desc="Left\nUpper Limb"]')
                await driver.pause(100)
                await lul.click()
                await driver.pause(100)
                await lul.click()
                break;
            case "Right Lower Limb":
                let rll = await $('//android.view.View[@content-desc="Right\nLower Limb"]')
                await driver.pause(100)
                await rll.click()
                await driver.pause(100)
                await rll.click()
                break;
            case "Left Lower Limb":
                let lll = await $('//android.view.View[@content-desc="Left\nLower Limb"]')
                await driver.pause(100)
                await lll.click()
                await driver.pause(100)
                await lll.click()
                break;
            default:
                break;
        }
    }
    async checkAddNewWound(){
        await $('//android.view.View[@content-desc="Add New Wound"]').isExisting();
    }
    async clickSpecificArea(area){
        let spec = await $(`//android.view.View[contains(@content-desc, "${area}")]`)
        await spec.click()
    }
    async fillUp(){

        await driver.touchAction([
            {action: 'press', x: 558, y: 1528},
            {action: 'moveTo', x: 534, y: 51},
            'release'
        ]);


        let woundtype = await $(`//android.widget.EditText[1]`)
        let exudateamount = await $(`//android.widget.EditText[2]`)

        await woundtype.click()
        await driver.pause(500)
        await woundtype.setValue("Skin Tear")
        await exudateamount.click()
        await driver.pause(500)
        await exudateamount.setValue("1 = Scant, moist wound tissue, no measurable drainage")


        await $('//android.widget.Button[3]').click()
        await $('//android.widget.ImageView[@content-desc="Shutter"]').click()
        await $('//android.widget.ImageButton[@content-desc="Done"]').click()
        await driver.pause(10000)
    }

}


const patients = new Patients()
export default patients