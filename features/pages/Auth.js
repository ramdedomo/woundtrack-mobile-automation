class Auth{
    async enterCredetials(user, pass){ // enter credentials from passed username and password
        let username = await $('//android.widget.EditText[1]')
        await username.click()
        await driver.pause(500)
        await username.setValue(user)

        let password = await $('//android.widget.EditText[2]')
        await password.click()
        await driver.pause(500)
        await password.setValue(pass)
    }
    async clickLogin(){
        await driver.pause(1000)
        await $('//android.widget.Button[@content-desc="Login"]').click()
    }
    async checkLogin(){
        await $('//android.view.View[@content-desc="Login to your account"]').isExisting();
    }
    async clickYes(){
        await $('//android.widget.Button[@content-desc="Yes"]').click()
    }
    async checkLogged(){
        let logged = await $('~Patients')
        await expect(logged).toBeDisplayed();
        await logged.isExisting();
    }
    async enableFingerprint(){
        let fingerprint = await $('//android.view.View[@content-desc="Fingerprint"]')
        await fingerprint.click()

        toggle = await $('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.Switch');
        await toggle.click()

        passwordInput = await $('//android.widget.EditText');
        await passwordInput.click();
        await driver.pause(500)
        await passwordInput.setValue('Tester2023!');

        await $('//android.widget.Button[@content-desc="Submit"]').click()
        await $('//android.widget.TextView[contains(@text, "Authentication required")]').isExisting();
    }
}

const auth = new Auth()
export default auth