class Setting{
    async clickInstruction(){
        let instruction =   await $('//android.view.View[@content-desc="Instruction"]')
        await instruction.click()
        await expect(instruction).toBeDisplayed();
        await instruction.isExisting();
    }
    async checkInstruction(){
        let instruction =   await $('//android.view.View[@content-desc="Instruction"]')
        await expect(instruction).toBeDisplayed();
        await instruction.isExisting();
        await driver.pause(3000)
    }
    async clickUserAgreement(){
        let user = await $('//android.view.View[@content-desc="User Agreement"]')
        await user.click()
    }
    async checkUserAgreement(){
        let userExist = await $('//android.view.View[@content-desc="Agreement"]')
        await expect(userExist).toBeDisplayed();
        await userExist.isExisting();
        await driver.pause(3000)
    }
    async clickLinktoDemo(){
        await $('//android.view.View[@content-desc="Link to Demo"]').click()
        await driver.pause(10000)
    }
    async clickBack(){
        await driver.back()
    }
}


const setting = new Setting()
export default setting