export default async function findSelector(selector){
    let user = await $(selector)
    let userExist = await user.isExisting()

    // scroll until the selector is present
    while (!userExist) {
        await driver.touchAction([
            {action: 'press', x: 150, y: 2000},
            {action: 'moveTo', x: 150, y: 700},
            'release'
        ]);

        userExist = await user.isExisting()
    }
}