import { Given, When, Then } from '@wdio/cucumber-framework';
import setting from '../pages/Settings.js';
import menu from '../pages/Menu.js';

    Given('Settings Tab',  async () => {
        await menu.clickMenuItems(2) // click settings
    })

    When('I click instruction',  async () => {
        await setting.clickInstruction() // open instruction
    })

    Then('a instruction page should show',  async () => {
        await setting.checkInstruction() // check if instruction is opened
        await setting.clickBack() // navigate back
    })

    When('I click user agreement',  async () => {
        await setting.clickUserAgreement() // open user agreement
    })
    
    Then('a user agreement page should show',  async () => {
        await setting.checkUserAgreement() // check if user agrerement is opened
        await setting.clickBack() // navigate back
    })

    When('I click link to demo',  async () => {
        await setting.clickLinktoDemo() // open link to demo 
    })
    
    Then('a link to demo video should play',  async () => {

    })
