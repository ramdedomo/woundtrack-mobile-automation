import { Given, When, Then } from '@wdio/cucumber-framework';
import patients from '../pages/Patients.js';
import auth from '../pages/Auth.js';

    let specificArea

    Given('I am on the Patient List', async () => {
        await patients.checkPatientList() // check patient list
    })
    
    When('i search for {string}', async (name) => {
        await patients.findPatient(name) // find patient
        await driver.pause(1000)
        await patients.clickPatient(name) // if found, click patient
    })
    
    When('I click the begin assessment', async () => {
        await patients.clickBeginAssessment() // click begin assessment
    })

    Then('a wound location page should show', async () => {
        await patients.checkTapArea() // check if wound location is present
    })

    When('i tap the area {string} specifically {string}', async (location, area) => {
        await patients.clickWoundArea(location) // tap the wound area 
        specificArea = area // store the selected area
    })

    When('I click the add assessment button', async () => {
        await patients.clickNewAssessment() // click new assessment
    })
    When('set a date to {string}', async (date) => {
        await patients.setDate(date) // set date
        await patients.openAssessment(date) // open assessment base from the given date
        await patients.newWoundYes() // add a new wound
    })

    Then('a add new wound page should show', async () => {
        await patients.checkAddNewWound() // check if a new wound page is present
    })

    When('i fill up the fields for adding a new wound', async () => {
        await patients.clickSpecificArea(specificArea) // click the area base from selected area
        await patients.fillUp()
    })

    Then('it should be filled up', async () => {
        
    })

    When('i edit the {string} assessment date and time to {string} and {string}', async (date, newdate, newtime) => {
      await patients.editAssessment(date, newdate, newtime) // edit date and time 
    })

    When('i go back to wound assessment log', async () => {
        //navigate to assessment logs
        await $('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View[1]/android.widget.Button').click()
    })

    Then('it should be updated', async () => {
      
    })

    When('i delete the assessment {string}', async (date) => {
        await patients.deleteAssessment(date) // delete assessment base from the given date
    })

    Then('it should be deleted', async () => {
      
    })


    When('I open {string} assessment', async (date) => {
        await patients.openAssessmentDetails(date) // find a assessment date and open
    })

    
    When('i add a new wound', async () => {
        await patients.newWoundYes() // new wound
    })

    When('i set the number {string} wound to healed', async (wound) => {
        
        let findWound =  await $(`//*[contains(@content-desc, "#${wound} - ")]`)
        let findWoundExist = await findWound.isExisting() // check wound is existing on viewport
    
        while(!findWoundExist){ // find the wound if not present on viewport
            await driver.touchAction([
                {action: 'press', x: 156, y: 1340},
                {action: 'moveTo', x: 156, y: 670},
                'release'
            ]);

            findWoundExist = await findWound.isExisting() // recheck
        }


        let hasimage =  await $(`//android.widget.ImageView[contains(@content-desc, "#${wound} - ")]/android.view.View[@content-desc="No"]`)
        let hasimageExist = await hasimage.isExisting() // check if wound contains image

        if(hasimageExist){
            await hasimage.click() // click the wound with image
        }else{
            await $(`//android.view.View[contains(@content-desc, "#${wound} - ")]/android.view.View[@content-desc="No"]`).click() // click the wound with no image
        }
       
        await driver.pause(1000) // wait
    })

    Then('it should be updated to healed', async () => {
      
    })


    

     

 

    
  



