import { Given, When, Then } from '@wdio/cucumber-framework';
import auth from '../pages/Auth.js';
import menu from '../pages/Menu.js';

    Given('I am on the login page', async () => {
        await auth.checkLogin() // check if at login page
    })

    When('I login with {string} and {string}', async (username, password) => {
        await auth.enterCredetials(username, password) // enter username password
        await auth.clickLogin() // click login
    })

    Then('i should be redirected to Patient list', async () => {
        await auth.checkLogged() // check if logged in
    })

    When('i click logout', async () => {
        driver.pause(5000) // wait 
        await menu.clickMenuItems(3) // click logout
        await auth.clickYes() // click yes
    })

    Then('i should be redirected to Login page', async () => {
        await auth.checkLogin() // check if at login page
    })

    When('i enable fingerprint', async () => {
        await menu.clickMenuItems(2) // click fingerprint
        await auth.enableFingerprint() // toggle fingerprint
    })

    When('I login using fingerprint', async () => {
        
    })


