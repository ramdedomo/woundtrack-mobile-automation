### Device Used:

- Nexus 5X
- Android 9.0
- 1080 x 1920

### How to run a test case

- 🔍 Find Tags on located on <b>features/e2e</b>.
![Tags Location](/images/image1.png "Image1")

- 🖥️ On cli, enter:
```
npm run test '@tag'
```
![Run Test](/images/image2.png "Image2")

- ⚙️ You can change the script on <b>package.json</b>.
```
  "test": "wdio ./wdio.conf.js --cucumberOpts.tagExpression",
```
![Change Script](/images/image3.png "Image3")
